﻿using UnityEngine;
using System.Collections;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;
using UnityEngine.UI;
using System;

public class AppodealServiceScript : MonoBehaviour
{


    static public AppodealServiceScript Instance { get { return _instance; } }
    static protected AppodealServiceScript _instance;
    public static string NO_BANNER_INTER_KEY = "noBannerInter";
    public static string NO_VIDEO_KEY = "noVideo";
    public static long timeInterval = 120;
    public GameObject panel;
    string appKey = "835f71d43c5a6557eac1b996c6196599cfe9873c7c3dad33";

    void Awake()
    {
        PlayerPrefs.SetString("prevInterTime", DateTime.Now.ToBinary().ToString());
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (_instance != this)
        {
            Destroy(gameObject);
        }
        Appodeal.disableLocationPermissionCheck();
        Init();
        //Appodeal.initialize(appKey, Appodeal.INTERSTITIAL | Appodeal.BANNER_VIEW);

        //ShowBanner();
        PlayerPrefs.SetInt("firstInterst", 0);
    }

    private bool isInitialized = false;

    public void Init()
    {
        if (!isInitialized)
        {
            if (PlayerPrefs.HasKey("result_gdpr"))
            {
                if (PlayerPrefs.GetInt("result_gdpr") == 1)
                {
                    Appodeal.initialize(appKey, Appodeal.INTERSTITIAL | Appodeal.BANNER_BOTTOM, true);
                    Debug.Log("appodealinit true");
                }
                else
                {
                    Appodeal.initialize(appKey, Appodeal.INTERSTITIAL | Appodeal.BANNER_BOTTOM, false);
                    Debug.Log("appodealinit false");
                }
                isInitialized = true;
            }
            else if (PlayerPrefs.GetInt("isNotNeedtoShowGPDR") == 1)
            {
                Appodeal.initialize(appKey, Appodeal.INTERSTITIAL | Appodeal.BANNER_BOTTOM, true);
                Debug.Log("appodealinit hasntkey");
                isInitialized = true;
            }
            Appodeal.initialize(appKey, Appodeal.INTERSTITIAL | Appodeal.BANNER_BOTTOM);
            ShowBanner();
        }
        else
        {
            Debug.Log("appodealinit fe");
        }

    }

    public void ShowInterstitial()
    {
        if (PlayerPrefs.GetInt(AppodealServiceScript.NO_BANNER_INTER_KEY, 0) == 0)
            Appodeal.show(Appodeal.INTERSTITIAL);
    }

    public void ShowInterstitialIfNeeded()
    {
        //Store the current time when it starts
        DateTime currentDate = DateTime.Now;

        //Grab the old time from the player prefs as a long
        long prevTimeShowed = Convert.ToInt64(PlayerPrefs.GetString("prevInterTime", "0"));

        //Convert the old time from binary to a DataTime variable
        DateTime oldDate = DateTime.FromBinary(prevTimeShowed);

        //Use the Subtract method and store the result as a timespan variable
        TimeSpan difference = currentDate.Subtract(oldDate);

        if (difference.TotalSeconds > timeInterval/*&&PlayerPrefs.GetInt("firstInterst")==1*/)
        {

            //Savee the current system time as a string in the player prefs class
            PlayerPrefs.SetString("prevInterTime", DateTime.Now.ToBinary().ToString());

            ShowInterstitial();
        }
    }

    public bool isInterstitialLoaded()
    {
        return Appodeal.isLoaded(Appodeal.INTERSTITIAL);
    }

    private bool isBannerShowed = false;

    public bool ShowBanner()
    {
        if (!isBannerShowed)
        {
            Debug.Log("show banner");
            isBannerShowed = true;
            return Appodeal.show(Appodeal.BANNER_BOTTOM);
        }
        else
        {
            return false;
        }
    }

    public bool ShowRewardedVideo()
    {
        return Appodeal.show(Appodeal.REWARDED_VIDEO);
        //      return false;
    }

    public bool isRewardedVideoLoaded()
    {
        return Appodeal.isLoaded(Appodeal.REWARDED_VIDEO);
    }

    public void DisableAds()
    {
        Debug.Log("disable banner");
        panel.SetActive(false);
        Appodeal.hide(Appodeal.BANNER_BOTTOM);
    }
}
