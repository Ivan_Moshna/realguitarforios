﻿using UnityEngine;
using System.Collections;

public class FlurryServiceScript : MonoBehaviour
{

#if UNITY_ANDROID
    private string FLURRY_API = "7QR9VDG7PT2ZK38RGGDC";
#elif UNITY_IPHONE
	private string FLURRY_API = "MYSB2HCSHBXBXX3H9FN7";
#else
	private string FLURRY_API = "x";
#endif
    public static FlurryServiceScript instance = null;
    void Start()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        FlurryAgent.Instance.onStartSession(FLURRY_API);

        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        //FlurryAgent.Instance.logEvent("Application is opened");
    }

    public void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            FlurryAgent.Instance.onEndSession();
        }
        else
        {
            FlurryAgent.Instance.onStartSession(FLURRY_API);
        }
    }

    public void LogFlurryEvent(string flurryEvent)
    {
        FlurryAgent.Instance.logEvent(flurryEvent);
    }
    public void OnApplicationQuit()
    {
        FlurryAgent.Instance.onEndSession();
    }
}
