﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeAudioScript : MonoBehaviour {

    public AudioSource mainAudSourse;

    AudioSource fadeAudSourse;

    private void Start()
    {
        fadeAudSourse = gameObject.GetComponent<AudioSource>();
        fadeAudSourse.outputAudioMixerGroup = mainAudSourse.outputAudioMixerGroup;
    }

    public void FadeAudio()
    {
        fadeAudSourse.clip = mainAudSourse.clip;
        fadeAudSourse.Play();
    }

}
