﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReplacePedalScript : MonoBehaviour {

    public static ReplacePedalScript Instance { get; private set; }



    public GameObject distortionPedal;
    public GameObject chorusPedal;
    public GameObject tremoloPedal;
    public GameObject delayPedal;

    public GameObject miniDistortionPedal;
    public GameObject miniChorusPedal;
    public GameObject miniTremoloPedal;

    public GameObject posOne;
    public GameObject posTwo;



    public void StratocasterSelected()
    {
       

        distortionPedal.SetActive(true);
        miniDistortionPedal.SetActive(true);
        chorusPedal.SetActive(true);
        miniChorusPedal.SetActive(true);
        tremoloPedal.SetActive(false);
        miniTremoloPedal.SetActive(false);

    }

    public void AcousticSelected()
    {
       

        distortionPedal.SetActive(false);
        miniDistortionPedal.SetActive(false);
        chorusPedal.SetActive(true);
        miniChorusPedal.SetActive(true);
        tremoloPedal.SetActive(true);
        miniTremoloPedal.SetActive(true);

        tremoloPedal.GetComponent<RectTransform>().position = posTwo.GetComponent<RectTransform>().position;
        miniTremoloPedal.GetComponent<RectTransform>().position = miniDistortionPedal.GetComponent<RectTransform>().position;
    }

    public void LesPaulSelected()
    {
       

        distortionPedal.SetActive(true);
        miniDistortionPedal.SetActive(true);
        chorusPedal.SetActive(false);
        miniChorusPedal.SetActive(false);
        tremoloPedal.SetActive(true);
        miniTremoloPedal.SetActive(true);


        tremoloPedal.GetComponent<RectTransform>().position = posOne.GetComponent<RectTransform>().position;
        miniTremoloPedal.GetComponent<RectTransform>().position = miniChorusPedal.GetComponent<RectTransform>().position;
    }


    void Start () {

        Instance = this;
	}
	

}
