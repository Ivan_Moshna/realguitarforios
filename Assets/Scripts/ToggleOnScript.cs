﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ToggleOnScript : MonoBehaviour
{
    public ToggleChangePositionScript changer;
    public SoundManagerScript choardLoader;

    

    private Color grey= new Color32(220, 221, 223, 255);
    private Color gold = new Color32(249, 183, 79, 255);

    public void OnToggleClick()
    {
        choardLoader.LoadChord(gameObject.name);
        changer.OnToggleChanges();
        if(gameObject.GetComponent<Toggle>().isOn)
        {
            gameObject.GetComponentInChildren<Text>().color = gold;
        }
        else
        {
            gameObject.GetComponentInChildren<Text>().color = grey;
        }
    }


}
