﻿using UnityEngine;

public class GDPRScript : MonoBehaviour
{
    void OnEnable()
    {
        //if (PlayerPrefs.HasKey("result_gdpr"))
        //{
        //    AppodealServiceScript.Instance.Init();
        //    //gameObject.SetActive(false);
        //}
    }

    public void OnYesClicked()
    {
        PlayerPrefs.SetInt("result_gdpr", 1);
        AppodealServiceScript.Instance.Init();
        gameObject.SetActive(false);
    }

    public void OnNoClicked()
    {
        PlayerPrefs.SetInt("result_gdpr", 0);
        AppodealServiceScript.Instance.Init();
        gameObject.SetActive(false);
    }


    

}
