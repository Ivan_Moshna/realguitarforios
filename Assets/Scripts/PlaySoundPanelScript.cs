﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class PlaySoundPanelScript : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public bool shouldPlay = false;

    public void OnPointerDown(PointerEventData eventData)
    {
        shouldPlay = true;
        
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        shouldPlay = false;
    }
}
