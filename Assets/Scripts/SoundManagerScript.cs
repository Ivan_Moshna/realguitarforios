﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class SoundManagerScript : MonoBehaviour {

    public static SoundManagerScript Instance { get; private set; }


    public string[] notes;
    public AudioClip[] samples = new AudioClip[6];
    public AudioSource[] secondSourses;

    Color32 normalColor = new Color32(255, 255, 255, 255);
    Color32 disableColor = new Color32(255, 255, 255, 100);

    public Image[] stringImage;

    public GameObject[] strings;
    public string chordNameChord;



    public void LoadChord(string chordName)
    {

        //string chordName = gameObject.name;
        Chord chord = ChordsGenerator.GetChord(chordName);
        for(int i=0; i<notes.Length; i++)
        {
            samples[i] = Resources.Load<AudioClip>(GameManager.Instance.pathName + chord.strings[i]);
            if (chord.strings[i] == "Z")
            {
                //strings[i].GetComponent<Image>().raycastTarget = false;
                
                strings[i].GetComponent<AudioSource>().clip = null;
                stringImage[i].color = disableColor;
            }
            else
            {

                strings[i].GetComponent<AudioSource>().clip = samples[i];

                stringImage[i].color = normalColor;
                //Debug.Log(samples[i]);
            }
                                  
        }
        chordNameChord = chordName;
        //Debug.Log("CHORD " + chordNameChord);
    }




    private void Start()
    {
        Instance = this;
        //LoadChord("open");
    }


}

