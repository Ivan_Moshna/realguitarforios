﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using DG.Tweening;
using UnityEngine.UI;


public class PedalManager : MonoBehaviour
{
    public AudioMixerGroup audMix;

    public GameObject additionPanelButton;
    public RectTransform additionChordPanel;

    public GameObject fxPanelButton;
    public RectTransform fxPanel;

    bool addPanelActive = false;
    bool fx = false;

    public Sprite activAddPanel;
    public Sprite inactivAddPanel;

    public Button addPanelButton;

    float timer;

    public GameObject rateUsPanel;

    
    public void OnAdditionButtonClick()
    {
        addPanelActive = !addPanelActive;

        
        

        if (AppodealServiceScript.Instance.isInterstitialLoaded() && timer > 180f)
        {
            AppodealServiceScript.Instance.ShowInterstitial();
            timer = 0;
        }
        if (addPanelActive)
        {
            addPanelButton.GetComponent<Image>().sprite = activAddPanel;

            additionChordPanel.DOAnchorPos(new Vector2(625f, 0f), 0.5f);
        }
        else
        {
            addPanelButton.GetComponent<Image>().sprite = inactivAddPanel;

            additionChordPanel.DOAnchorPos(new Vector2(1920f, 0f), 0.5f);
        }
    }

    public void OnFXButtonClick()
    {
        fx = !fx;
        if (AppodealServiceScript.Instance.isInterstitialLoaded() && timer > 180f)
        {
            AppodealServiceScript.Instance.ShowInterstitial();
            timer = 0;
        }
        if (fx)
        {
            //if (additionChordPanel.active)
            //{
            //    additionChordPanel.SetActive(false);
            //    addPanel = false;
            //}
            fxPanel.DOAnchorPos(new Vector2(0f, 0f), 0.5f);
        }
        else
        {
            fxPanel.DOAnchorPos(new Vector2(1920f, 0f), 0.5f);
        }
    }


    public void ValuesOfMixerEffects(float volume, float distTone, float distLevel, float chorusRate, float chorusDepth, float chorusMix, float delayFX, float feedbackFX, float mixDelayFX, float distLowTone, float mixTremoloDepth, float mixTremoloRate)
    {
        audMix.audioMixer.SetFloat("volumeOfMaster", volume);
        audMix.audioMixer.SetFloat("distortionTone", distTone);
        audMix.audioMixer.SetFloat("distortionLevel", distLevel);
        audMix.audioMixer.SetFloat("chorusFXRate", chorusRate);
        audMix.audioMixer.SetFloat("chorusFXDepth", chorusDepth);
        audMix.audioMixer.SetFloat("chorusFXMix", chorusMix);
        audMix.audioMixer.SetFloat("delayFXdelay", delayFX);
        audMix.audioMixer.SetFloat("feedbackFXdelay", feedbackFX);
        audMix.audioMixer.SetFloat("mixFXdelay", mixDelayFX);
        audMix.audioMixer.SetFloat("lowPassTone", distLowTone);
        audMix.audioMixer.SetFloat("tremoloDepth", mixTremoloDepth);
        audMix.audioMixer.SetFloat("tremoloRate", mixTremoloRate);
        
    }
    
    public void ValuesOfDistortionEffect(float volume, float distTone, float distLevel, float distLowTone)
    {
        audMix.audioMixer.SetFloat("volumeOfMaster", volume);
        audMix.audioMixer.SetFloat("distortionTone", distTone);
        audMix.audioMixer.SetFloat("distortionLevel", distLevel);
        audMix.audioMixer.SetFloat("lowPassTone", distLowTone);

    }

    public void ValuesOfChorusEffect(float chorusRate, float chorusDepth, float chorusMix)
    {
        audMix.audioMixer.SetFloat("chorusFXRate", chorusRate);
        audMix.audioMixer.SetFloat("chorusFXDepth", chorusDepth);
        audMix.audioMixer.SetFloat("chorusFXMix", chorusMix);
    }

    public void ValuesOfDelayEffect(float delayFX, float feedbackFX, float mixDelayFX)
    {
        audMix.audioMixer.SetFloat("delayFXdelay", delayFX);
        audMix.audioMixer.SetFloat("feedbackFXdelay", feedbackFX);
        audMix.audioMixer.SetFloat("mixFXdelay", mixDelayFX);
    }

    public void ValuesOfTremoloEffect(float tremoloDepthFX, float tremoloRateFX)
    {
        audMix.audioMixer.SetFloat("tremoloDepth", tremoloDepthFX);
        audMix.audioMixer.SetFloat("tremoloRate", tremoloRateFX);
    }


    private void Start()
    {
        additionChordPanel.DOAnchorPos(new Vector2(1920f, 0f), 0.5f);

        fxPanel.DOAnchorPos(new Vector2(1920f, 0f), 0.5f);


        ValuesOfMixerEffects(0f,-80f, 0.5f, 0f, 0.45f, 0.5f, 0.5f, 0f, 0f, 2500f, 0, 0);

        timer = 0;

        addPanelButton.GetComponent<Image>().sprite = inactivAddPanel;
    }

    private void Update()
    {
        timer += Time.deltaTime;

        if(Input.GetKeyDown(KeyCode.Escape) && fx == true)
        {
            fx = !fx;
            fxPanel.DOAnchorPos(new Vector2(1920f, 0f), 0.5f);

        }

        else if(Input.GetKeyDown(KeyCode.Escape) && addPanelActive == true)
        {
            addPanelActive = !addPanelActive;
            addPanelButton.GetComponent<Image>().sprite = inactivAddPanel;

            additionChordPanel.DOAnchorPos(new Vector2(1920f, 0f), 0.5f);
        }
        else if (Input.GetKeyDown(KeyCode.Escape) && GameManager.Instance.rateUsPanel.activeSelf == true)
        {
            GameManager.Instance.OnLaterRateUsButtonClick();
        }

        else if (Input.GetKeyDown(KeyCode.Escape) && GameManager.Instance.startPanel.activeSelf == false)
        {
            GameManager.Instance.OnChooseButtonClick();
        }

        else if (Input.GetKeyDown(KeyCode.Escape) && GameManager.Instance.startPanel.activeSelf == true)
        {
            Application.Quit();
        }
        
        
        
    }
}
