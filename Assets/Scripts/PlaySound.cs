﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class PlaySound : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler, IPointerUpHandler
{
    public PlaySoundPanelScript playPanel;
    public int id;

    public GameObject secondAudSourse;

    public Animator anim;

    AudioSource audSourse;


    public void OnPointerDown(PointerEventData eventData)
    {
        playPanel.shouldPlay = true;
        
        //audSourse.Play();
        secondAudSourse.GetComponent<FadeAudioScript>().FadeAudio();
        //if (gameObject.GetComponent<AudioSource>().isPlaying)
        //{
        //    anim.SetBool("isPlaying", true);
        //}
        if(secondAudSourse.GetComponent<AudioSource>().isPlaying)
        {
            anim.SetBool("isPlaying", true);
        }

    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (playPanel.shouldPlay)
        {
            //audSourse.Play();
            secondAudSourse.GetComponent<FadeAudioScript>().FadeAudio();


            //if (gameObject.GetComponent<AudioSource>().isPlaying)
            //{
            //    anim.SetBool("isPlaying", true);
            //}
            if (secondAudSourse.GetComponent<AudioSource>().isPlaying)
            {
                anim.SetBool("isPlaying", true);
            }

        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        playPanel.shouldPlay = false;
    }

    //public void GetAnimation()
    //{
    //    if (gameObject.GetComponent<AudioSource>().isPlaying)
    //    {
    //        anim.SetBool("isPlaying", true);
    //    }

    //    if (!gameObject.GetComponent<AudioSource>().isPlaying)
    //    {
    //        anim.SetBool("isPlaying", false);

    //    }
    //}

    private void Start()
    {
        anim.SetBool("isPlaying", false);
        audSourse = gameObject.GetComponent<AudioSource>();
    }

    private void Update()
    {
        //if (!audSourse.isPlaying)
        //{
        //    anim.SetBool("isPlaying", false);
        //}
        if (!secondAudSourse.GetComponent<AudioSource>().isPlaying)
        {
            anim.SetBool("isPlaying", false);
        }
    }
}
