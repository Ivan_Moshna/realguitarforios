﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ToggleChangePositionScript : MonoBehaviour {

    Transform currentToggle;
    Transform additionToggle;
    public SoundManagerScript sms;
    //public string newChordName;
    public ToggleGroup mToggleGroup, aToggleGroup;

    public void OnToggleChanges()
    {
        if (mToggleGroup.AnyTogglesOn() && aToggleGroup.AnyTogglesOn())
        {
            SwapToggles();
        }
        else if (!mToggleGroup.AnyTogglesOn() && !aToggleGroup.AnyTogglesOn())
        {
            sms.LoadChord("open");
        }
    }

    private void SwapToggles()
    {
        Toggle m=null, a=null;
        foreach (Toggle t in mToggleGroup.ActiveToggles())
        {
            if (t.isOn)
            {
                m = t;
                break;
            }
        }
        foreach (Toggle t in aToggleGroup.ActiveToggles())
        {
            if (t.isOn)
            {
                a = t;
                break;
            }
        }
        if (m != null && a != null)
        {
            m.gameObject.name = a.gameObject.name;
            m.GetComponentInChildren<Text>().text = a.GetComponentInChildren<Text>().text;
            a.isOn = false;
        }
    }
 }
