﻿using UnityEngine;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using System;


/// <summary>
/// The Geo data for a user.
/// 
/// http://ip-api.com/docs/api:json
/// 
/// <code>
/// {
/// 	"status": "success",
/// 	"country": "COUNTRY",
/// 	"countryCode": "COUNTRY CODE",
/// 	"region": "REGION CODE",
/// 	"regionName": "REGION NAME",
/// 	"city": "CITY",
/// 	"zip": "ZIP CODE",
/// 	"lat": LATITUDE,
/// 	"lon": LONGITUDE,
/// 	"timezone": "TIME ZONE",
/// 	"isp": "ISP NAME",
/// 	"org": "ORGANIZATION NAME",
/// 	"as": "AS NUMBER / NAME",
/// 	"query": "IP ADDRESS USED FOR QUERY"
/// }
/// </code>
/// 
/// </summary>
[System.Serializable]
public class GeoData
{
    public string countryCode;
}

public class GeoCountry : MonoBehaviour
{
    void Start()
    {
        if (!PlayerPrefs.HasKey("result_gdpr"))
            StartCoroutine(GetText());
    }
    private string[] countries = { "BE", "EL", "LT", "PT", "BG", "ES", "LU", "RO", "CZ", "FR", "HU", "SI", "DK", "HR", "MT", "SK", "DE", "IT", "NL", "FI", "EE", "CY", "AT", "SE", "IE", "LV", "PL", "UK", "CH", "NO", "IS", "LI" };
    public GameObject gpdrPanel;

    IEnumerator GetText()
    {
        UnityWebRequest www = UnityWebRequest.Get("http://ip-api.com/json");
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            if (!PlayerPrefs.HasKey("result_gdpr"))
            {
                gpdrPanel.SetActive(true);
            }  
            Debug.Log(www.error);

        }
        else
        {
            // Show results as text
            Debug.Log(www.downloadHandler.text);

            // Or retrieve results as binary data
            byte[] results = www.downloadHandler.data;
            string s = www.downloadHandler.text;
            try
            {
                GeoData data = JsonUtility.FromJson<GeoData>(s);
                bool isNeedToShow = false;
                foreach (string country in countries)
                {
                    if (country.CompareTo(data.countryCode) == 0)
                    {
                        isNeedToShow = true;
                    }
                }
                if (isNeedToShow)
                {
                    if (!PlayerPrefs.HasKey("result_gdpr"))
                    {
                        gpdrPanel.SetActive(true);
                    }

                }
                else
                {
                    PlayerPrefs.SetInt("isNotNeedtoShowGPDR", 1);
                    AppodealServiceScript.Instance.Init();
                }
            }
            catch (Exception e)
            {
                if (!PlayerPrefs.HasKey("result_gdpr"))
                {
                    gpdrPanel.SetActive(true);
                }
                }
            }
    }

    public void OnPPButtonClick()
    {
        Debug.Log("asdasd");
        //if (PlayerPrefs.HasKey("result_gdpr"))
        if (PlayerPrefs.GetInt("isNotNeedtoShowGPDR", 0) != 1)
        {
            gpdrPanel.SetActive(true);
        }
        else
        {
            Application.OpenURL("http://2d2b.github.io/privacy/com.mobiray.realguitar3.html");
        }

    }

    public void OnPPButtonClickOnly()
    {
        Application.OpenURL("http://2d2b.github.io/privacy/com.mobiray.realguitar3.html");
    }
}
