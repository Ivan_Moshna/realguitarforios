﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChordsCreatorGUI : MonoBehaviour {

    public SoundManagerScript sms;
    public ToggleChangePositionScript tcps;
    public GameObject chordPrefab;
    public VerticalLayoutGroup[] vlgs;
    public ToggleGroup tg;

    private void Start()
    {
        for (int i = 0; i < 10; i++)
        {
            for (int j = 0; j < 7; j++)
            {
                GameObject g = Instantiate(chordPrefab);
                g.gameObject.transform.SetParent( vlgs[i].transform);
                g.GetComponent<ToggleOnScript>().changer = tcps;
                g.GetComponent<ToggleOnScript>().choardLoader = sms;
                g.name = ChordsGenerator.chords[j * 10 + i].name;
                g.GetComponent<RectTransform>().localScale = Vector3.one;
                g.GetComponentInChildren<Text>().text= ChordsGenerator.chords[j * 10 + i].name;
                g.GetComponent<Toggle>().group = tg;
            }
        }
    }
}
