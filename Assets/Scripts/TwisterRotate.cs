﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TwisterRotate : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler
{
    Vector2 currentPos;
    //public float changerZ;
    private float minPosAngle = 1f;
    private float maxPosAngle = 270f;

    public float setChanger;

    public GameObject pedalManager;
    public Toggle distortionToggle;
    public Toggle chorusToggle;
    public Toggle delayToggle;
    public Toggle tremoloToggle;


    public float twisterSpeed;
    public string nameTwister;

    private static float volumeLevel;
    private static float distortionTone;
    private static float distortionLevel;
    private static float lowPass;
    private static float chorusRate;
    private static float chorusDepth;
    private static float chorusMix;
    private static float delayDelay;
    private static float feedbackDelay;
    private static float mixDelay;
    private static float tremoloRate;
    private static float tremoloDepth;


    private float startVolume = 0f;
    private float startDist = 0.5f;
    private float startTone = -80f;
    private float startRate = 0f;
    private float startDepth = 0.45f;
    private float startMixChorus = 0.5f;
    private float startDelay = 0.5f;
    private float startFeedback = 0f;
    private float startMixDelay = 0f;
    private float startTremoloDepth = 0f;
    private float startTremoloRate = 0f;

    private float startAngle;

    public float volumeCoef;
     



    public void OnPointerDown(PointerEventData eventData)
    {
        currentPos = eventData.pressPosition;
        //Debug.Log("startAngle " + startAngle);
    }

    public void OnDrag(PointerEventData eventData)
    {
        
        if (eventData.position.y > currentPos.y)
        {
            if (startAngle + (eventData.position.y - currentPos.y) * twisterSpeed >= 270)
            {
                
               // Debug.Log(startAngle + (eventData.position.y - currentPos.y) * twisterSpeed);

                //gameObject.transform.Rotate(0f, 0f, 0f);
                gameObject.transform.eulerAngles = new Vector3(0, 0, 90);

                //Debug.Log("исключение");

                if (nameTwister == "lowPassTone")
                {
                    lowPass = 5000f;
                    if (distortionToggle.isOn)
                        pedalManager.GetComponent<PedalManager>().ValuesOfDistortionEffect(volumeLevel, distortionTone, distortionLevel, lowPass);
                }
                else if (nameTwister == "distortionLevel")
                {
                    distortionTone = 0f;
                    distortionLevel = 0.95f;
                    volumeLevel = -10f;
                    if (distortionToggle.isOn)
                        pedalManager.GetComponent<PedalManager>().ValuesOfDistortionEffect(volumeLevel, distortionTone, distortionLevel, lowPass);
                }
                //else if (nameTwister == "volume")
                //{
                //    volumeLevel = 20f;
                //    if (distortionToggle.isOn)
                //        pedalManager.GetComponent<PedalManager>().ValuesOfDistortionEffect(volumeLevel, distortionTone, distortionLevel, lowPass);
                //}
                else if (nameTwister == "chorusRate")
                {
                    chorusRate = 10f;
                    if (chorusToggle.isOn)
                        pedalManager.GetComponent<PedalManager>().ValuesOfChorusEffect(chorusRate, chorusDepth, chorusMix);
                }
                else if (nameTwister == "chorusDepth")
                {
                    chorusDepth = 1f;
                    if (chorusToggle.isOn)
                        pedalManager.GetComponent<PedalManager>().ValuesOfChorusEffect(chorusRate, chorusDepth, chorusMix);
                }
                else if (nameTwister == "chorusMix")
                {
                    chorusMix = 1f;
                    if (chorusToggle.isOn)
                        pedalManager.GetComponent<PedalManager>().ValuesOfChorusEffect(chorusRate, chorusDepth, chorusMix);
                }
                else if (nameTwister == "delayDelay")
                {
                    delayDelay = 2000f;
                    if (delayToggle.isOn)
                        pedalManager.GetComponent<PedalManager>().ValuesOfDelayEffect(delayDelay, feedbackDelay, mixDelay);
                }
                else if (nameTwister == "feedbackDelay")
                {
                    feedbackDelay = 1f;
                    if (delayToggle.isOn)
                        pedalManager.GetComponent<PedalManager>().ValuesOfDelayEffect(delayDelay, feedbackDelay, mixDelay);
                }
                else if (nameTwister == "delayMix")
                {
                    mixDelay = 1f;
                    if (delayToggle.isOn)
                        pedalManager.GetComponent<PedalManager>().ValuesOfDelayEffect(delayDelay, feedbackDelay, mixDelay);
                }
                else if(nameTwister == "tremoloDepth")
                {
                    tremoloDepth = 0.5f;
                    if (tremoloToggle.isOn)
                        pedalManager.GetComponent<PedalManager>().ValuesOfTremoloEffect(tremoloDepth, tremoloRate);
                }
                else if(nameTwister == "tremoloRate")
                {
                    tremoloRate = 15f;
                    if (tremoloToggle.isOn)
                        pedalManager.GetComponent<PedalManager>().ValuesOfTremoloEffect(tremoloDepth, tremoloRate);
                }

            }
            else
            {
                gameObject.transform.eulerAngles = new Vector3(0, 0, -(startAngle + (eventData.position.y - currentPos.y) * twisterSpeed));

                //Debug.Log(gameObject.transform.eulerAngles.z);
                if (nameTwister == "lowPassTone")
                {
                    lowPass = Mathf.Abs((gameObject.transform.eulerAngles.z - 360) / 270) * 5000+1000;
                    //Debug.Log(distortionTone);
                    if (distortionToggle.isOn)
                        pedalManager.GetComponent<PedalManager>().ValuesOfDistortionEffect(volumeLevel, distortionTone, distortionLevel, lowPass);
                }
                else if (nameTwister == "distortionLevel")
                {
                    distortionTone = Mathf.Abs((gameObject.transform.eulerAngles.z - 360) / 270) * 80 - 80;
                    distortionLevel = Mathf.Abs((gameObject.transform.eulerAngles.z - 360) / 270);
                    volumeLevel = -distortionLevel * volumeCoef;
                    if (distortionToggle.isOn)
                        pedalManager.GetComponent<PedalManager>().ValuesOfDistortionEffect(volumeLevel, distortionTone, distortionLevel, lowPass);
                }
                //else if (nameTwister == "volume")
                //{
                //    volumeLevel = Mathf.Abs((gameObject.transform.eulerAngles.z - 360) / 270) * 100 - 80;
                //    if (distortionToggle.isOn)
                //        pedalManager.GetComponent<PedalManager>().ValuesOfDistortionEffect(volumeLevel, distortionTone, distortionLevel, lowPass);
                //}
                else if (nameTwister == "chorusRate")
                {
                    chorusRate = Mathf.Abs((gameObject.transform.eulerAngles.z - 360) / 270) * 10;
                    if (chorusToggle.isOn)
                        pedalManager.GetComponent<PedalManager>().ValuesOfChorusEffect(chorusRate, chorusDepth, chorusMix);
                }
                else if (nameTwister == "chorusDepth")
                {
                    chorusDepth = Mathf.Abs((gameObject.transform.eulerAngles.z - 360) / 270);
                    if (chorusToggle.isOn)
                        pedalManager.GetComponent<PedalManager>().ValuesOfChorusEffect(chorusRate, chorusDepth, chorusMix);
                }
                else if (nameTwister == "chorusMix")
                {
                    chorusMix = Mathf.Abs((gameObject.transform.eulerAngles.z - 360) / 270);
                    if (chorusToggle.isOn)
                        pedalManager.GetComponent<PedalManager>().ValuesOfChorusEffect(chorusRate, chorusDepth, chorusMix);
                }
                else if (nameTwister == "delayDelay")
                {
                    delayDelay = Mathf.Abs((gameObject.transform.eulerAngles.z - 360) / 270)*2000;
                    if (delayToggle.isOn)
                        pedalManager.GetComponent<PedalManager>().ValuesOfDelayEffect(delayDelay, feedbackDelay, mixDelay);
                }
                else if (nameTwister == "feedbackDelay")
                {
                    feedbackDelay = Mathf.Abs((gameObject.transform.eulerAngles.z - 360) / 270);
                    if (delayToggle.isOn)
                        pedalManager.GetComponent<PedalManager>().ValuesOfDelayEffect(delayDelay, feedbackDelay, mixDelay);
                }
                else if (nameTwister == "delayMix")
                {
                    mixDelay = Mathf.Abs((gameObject.transform.eulerAngles.z - 360) / 270);
                    if (delayToggle.isOn)
                        pedalManager.GetComponent<PedalManager>().ValuesOfDelayEffect(delayDelay, feedbackDelay, mixDelay);
                }
                else if(nameTwister == "tremoloDepth")
                {
                    tremoloDepth = Mathf.Abs((gameObject.transform.eulerAngles.z - 360) / 270)*0.5f;
                    if (tremoloToggle.isOn)
                        pedalManager.GetComponent<PedalManager>().ValuesOfTremoloEffect(tremoloDepth, tremoloRate);
                }
                else if(nameTwister == "tremoloRate")
                {
                    tremoloRate = Mathf.Abs((gameObject.transform.eulerAngles.z - 360) / 270)*15;
                    if (tremoloToggle.isOn)
                        pedalManager.GetComponent<PedalManager>().ValuesOfTremoloEffect(tremoloDepth, tremoloRate);
                }
                //SetAngle(-twisterSpeed);
            }
        }

        if (eventData.position.y < currentPos.y)
        {

            if (startAngle + (eventData.position.y - currentPos.y) * twisterSpeed <= 1)
            {
                gameObject.transform.Rotate(0f, 0f, 0f);
                //gameObject.transform.eulerAngles = new Vector3(0, 0, 0);

                if (nameTwister == "lowPassTone")
                {
                    lowPass = 1000f;
                    if (distortionToggle.isOn)
                        pedalManager.GetComponent<PedalManager>().ValuesOfDistortionEffect(volumeLevel, distortionTone, distortionLevel, lowPass);
                }
                else if (nameTwister == "distortionLevel")
                {
                    distortionTone = -80f;
                    distortionLevel = 0f;
                    volumeLevel = 0f;
                    if (distortionToggle.isOn)
                        pedalManager.GetComponent<PedalManager>().ValuesOfDistortionEffect(volumeLevel, distortionTone, distortionLevel, lowPass);
                }
                //else if (nameTwister == "volume")
                //{
                //    volumeLevel = -80f;
                //    if (distortionToggle.isOn)
                //        pedalManager.GetComponent<PedalManager>().ValuesOfDistortionEffect(volumeLevel, distortionTone, distortionLevel, lowPass);
                //}
                else if (nameTwister == "chorusRate")
                {
                    chorusRate = 0.1f;
                    if (chorusToggle.isOn)
                        pedalManager.GetComponent<PedalManager>().ValuesOfChorusEffect(chorusRate, chorusDepth, chorusMix);
                }
                else if (nameTwister == "chorusDepth")
                {
                    chorusDepth = 0f;
                    if (chorusToggle.isOn)
                        pedalManager.GetComponent<PedalManager>().ValuesOfChorusEffect(chorusRate, chorusDepth, chorusMix);
                }
                else if (nameTwister == "chorusMix")
                {
                    chorusMix = 0f;
                    if (chorusToggle.isOn)
                        pedalManager.GetComponent<PedalManager>().ValuesOfChorusEffect(chorusRate, chorusDepth, chorusMix);
                }
                else if (nameTwister == "delayDelay")
                {
                    delayDelay = 0f;
                    if (delayToggle.isOn)
                        pedalManager.GetComponent<PedalManager>().ValuesOfDelayEffect(delayDelay, feedbackDelay, mixDelay);
                }
                else if (nameTwister == "feedbackDelay")
                {
                    feedbackDelay = 0;
                    if (delayToggle.isOn)
                        pedalManager.GetComponent<PedalManager>().ValuesOfDelayEffect(delayDelay, feedbackDelay, mixDelay);
                }
                else if (nameTwister == "delayMix")
                {
                    mixDelay = 0;
                    if (delayToggle.isOn)
                        pedalManager.GetComponent<PedalManager>().ValuesOfDelayEffect(delayDelay, feedbackDelay, mixDelay);
                }
                else if(nameTwister == "tremoloDepth")
                {
                    tremoloDepth = 0;
                    if (tremoloToggle.isOn)
                        pedalManager.GetComponent<PedalManager>().ValuesOfTremoloEffect(tremoloDepth, tremoloRate);
                }
                else if(nameTwister == "tremoloRate")
                {
                    tremoloRate = 0;
                    if (tremoloToggle.isOn)
                        pedalManager.GetComponent<PedalManager>().ValuesOfTremoloEffect(tremoloDepth, tremoloRate);
                }
            }
            else
            {
                gameObject.transform.eulerAngles = new Vector3(0, 0, -(startAngle + (eventData.position.y - currentPos.y) * twisterSpeed));


                if (nameTwister == "lowPassTone")
                {
                    lowPass = Mathf.Abs((gameObject.transform.eulerAngles.z - 360) / 270) * 5000;
                    if (distortionToggle.isOn)
                        pedalManager.GetComponent<PedalManager>().ValuesOfDistortionEffect(volumeLevel, distortionTone, distortionLevel, lowPass);
                }
                else if (nameTwister == "distortionLevel")
                {
                    distortionTone = Mathf.Abs((gameObject.transform.eulerAngles.z - 360) / 270) * 80 - 80;
                    distortionLevel = Mathf.Abs((gameObject.transform.eulerAngles.z - 360) / 270);
                    volumeLevel = -distortionLevel*volumeCoef;
                    if (distortionToggle.isOn)
                        pedalManager.GetComponent<PedalManager>().ValuesOfDistortionEffect(volumeLevel, distortionTone, distortionLevel, lowPass);
                }
                //else if (nameTwister == "volume")
                //{
                //    volumeLevel = Mathf.Abs((gameObject.transform.eulerAngles.z - 360) / 270) * 100 - 80;
                //    if (distortionToggle.isOn)
                //        pedalManager.GetComponent<PedalManager>().ValuesOfDistortionEffect(volumeLevel, distortionTone, distortionLevel, lowPass);
                //}
                else if (nameTwister == "chorusRate")
                {
                    chorusRate = Mathf.Abs((gameObject.transform.eulerAngles.z - 360) / 270) * 10;
                    if (chorusToggle.isOn)
                        pedalManager.GetComponent<PedalManager>().ValuesOfChorusEffect(chorusRate, chorusDepth, chorusMix);
                }
                else if (nameTwister == "chorusDepth")
                {
                    chorusDepth = Mathf.Abs((gameObject.transform.eulerAngles.z - 360) / 270);
                    if (chorusToggle.isOn)
                        pedalManager.GetComponent<PedalManager>().ValuesOfChorusEffect(chorusRate, chorusDepth, chorusMix);
                }
                else if (nameTwister == "chorusMix")
                {
                     chorusMix = Mathf.Abs((gameObject.transform.eulerAngles.z - 360) / 270);
                    if (chorusToggle.isOn)
                        pedalManager.GetComponent<PedalManager>().ValuesOfChorusEffect(chorusRate, chorusDepth, chorusMix);
                }
                else if (nameTwister == "delayDelay")
                {
                    delayDelay = Mathf.Abs((gameObject.transform.eulerAngles.z - 360) / 270) * 2000;
                    if (delayToggle.isOn)
                        pedalManager.GetComponent<PedalManager>().ValuesOfDelayEffect(delayDelay, feedbackDelay, mixDelay);
                }
                else if (nameTwister == "feedbackDelay")
                {
                    feedbackDelay = Mathf.Abs((gameObject.transform.eulerAngles.z - 360) / 270);
                    if (delayToggle.isOn)
                        pedalManager.GetComponent<PedalManager>().ValuesOfDelayEffect(delayDelay, feedbackDelay, mixDelay);
                }
                else if (nameTwister == "delayMix")
                {
                    mixDelay = Mathf.Abs((gameObject.transform.eulerAngles.z - 360) / 270);
                    if (delayToggle.isOn)
                        pedalManager.GetComponent<PedalManager>().ValuesOfDelayEffect(delayDelay, feedbackDelay, mixDelay);
                }
                else if(nameTwister == "tremoloDepth")
                {
                    tremoloDepth = Mathf.Abs((gameObject.transform.eulerAngles.z - 360) / 270)*0.5f;
                    if (tremoloToggle.isOn)
                        pedalManager.GetComponent<PedalManager>().ValuesOfTremoloEffect(tremoloDepth, tremoloRate);
                }
                else if(nameTwister == "tremoloRate")
                {
                    tremoloRate = Mathf.Abs((gameObject.transform.eulerAngles.z - 360) / 270)*15;
                    if (tremoloToggle.isOn)
                        pedalManager.GetComponent<PedalManager>().ValuesOfTremoloEffect(tremoloDepth, tremoloRate);
                }
                //SetAngle(twisterSpeed);
                }
            }
        }
    public void OnPointerUp(PointerEventData eventData)
    {
        startAngle = 360 - gameObject.transform.eulerAngles.z;
        if (startAngle == 360)
        {
            startAngle = 0;
        }
    }

    public void OnDistortionToggle()
    {
        if (distortionToggle.isOn)
        {

            //pedalManager.GetComponent<PedalManager>().ValuesOfMixerEffects(volumeLevel, distortionTone, distortionLevel, chorusRate, chorusDepth, chorusMix, delayDelay, feedbackDelay, mixDelay);
            pedalManager.GetComponent<PedalManager>().ValuesOfDistortionEffect(volumeLevel, distortionTone, distortionLevel, lowPass);
        }
        else
        {
            pedalManager.GetComponent<PedalManager>().ValuesOfDistortionEffect(startVolume, startTone, startDist, lowPass);
            //pedalManager.GetComponent<PedalManager>().ValuesOfMixerEffects(startVolume, startTone, startDist, chorusRate, chorusDepth, chorusMix, delayDelay, feedbackDelay, mixDelay);
            //volumeLevel = startVolume;
            //distortionTone = startTone;
            //distortionLevel = startDist;
        }
    }
    
    public void OnChorusToggle()
    {
        if(chorusToggle.isOn)
        {
            pedalManager.GetComponent<PedalManager>().ValuesOfChorusEffect(chorusRate, chorusDepth, chorusMix);
            //pedalManager.GetComponent<PedalManager>().ValuesOfMixerEffects(volumeLevel, distortionTone, distortionLevel, chorusRate, chorusDepth, chorusMix, delayDelay, feedbackDelay, mixDelay);
        }
        else
        {
            pedalManager.GetComponent<PedalManager>().ValuesOfChorusEffect(startRate, startDepth, startMixChorus);
            //pedalManager.GetComponent<PedalManager>().ValuesOfMixerEffects(volumeLevel, distortionTone, distortionLevel, startRate, startDepth, startMixChorus, delayDelay, feedbackDelay, mixDelay);
            //chorusRate = startRate;
            //chorusDepth = startDepth;
            //chorusMix = startMixChorus;
        }
    }

    public void OnDelayToggle()
    {
        if (delayToggle.isOn)
        {
            //pedalManager.GetComponent<PedalManager>().ValuesOfMixerEffects(volumeLevel, distortionTone, distortionLevel, chorusRate, chorusDepth, chorusMix, delayDelay, feedbackDelay, mixDelay);
            pedalManager.GetComponent<PedalManager>().ValuesOfDelayEffect(delayDelay, feedbackDelay, mixDelay);

        }
        else
        {
            pedalManager.GetComponent<PedalManager>().ValuesOfDelayEffect(startDelay, startFeedback, startMixDelay);
            //pedalManager.GetComponent<PedalManager>().ValuesOfMixerEffects(volumeLevel, distortionTone, distortionLevel, chorusRate, chorusDepth, chorusMix, startDelay, startFeedback, startMixDelay);
        }
    }

    public void OnTremoloToggle()
    {
        if(tremoloToggle.isOn)
        {
            pedalManager.GetComponent<PedalManager>().ValuesOfTremoloEffect(tremoloDepth, tremoloRate);
        }
        else
        {
            pedalManager.GetComponent<PedalManager>().ValuesOfTremoloEffect(startTremoloDepth, startTremoloRate);
        }
    }

    public void SetAngle(float angle)
    {
        gameObject.transform.Rotate(0, 0, angle);

        //gameObject.transform.eulerAngles = new Vector3(0f, 0f, angle);
        //return angle;
    }

    private void Start()
    {
        //distortionTone = -80f;
        //volumeLevel = -10;
        startAngle = 360 - gameObject.transform.eulerAngles.z;


        if (nameTwister == "lowPassTone")
        {
            lowPass = Mathf.Abs((gameObject.transform.eulerAngles.z - 360) / 270) * 5000+1000;
            if (distortionToggle.isOn)
                pedalManager.GetComponent<PedalManager>().ValuesOfDistortionEffect(volumeLevel, distortionTone, distortionLevel, lowPass);
        }
        else if (nameTwister == "distortionLevel")
        {
            distortionTone = Mathf.Abs((gameObject.transform.eulerAngles.z - 360) / 270) * 80 - 80;
            distortionLevel = Mathf.Abs((gameObject.transform.eulerAngles.z - 360) / 270);
            volumeLevel = -distortionLevel*volumeCoef;
            if (distortionToggle.isOn)
                pedalManager.GetComponent<PedalManager>().ValuesOfDistortionEffect(volumeLevel, distortionTone, distortionLevel, lowPass);
        }
        else if (nameTwister == "volume")
        {
            volumeLevel = Mathf.Abs((gameObject.transform.eulerAngles.z - 360) / 270) * 100 - 80;
            if (distortionToggle.isOn)
                pedalManager.GetComponent<PedalManager>().ValuesOfDistortionEffect(volumeLevel, distortionTone, distortionLevel, lowPass);
        }
        else if (nameTwister == "chorusRate")
        {
            chorusRate = Mathf.Abs((gameObject.transform.eulerAngles.z - 360) / 270) * 20;
            if (chorusToggle.isOn)
                pedalManager.GetComponent<PedalManager>().ValuesOfChorusEffect(chorusRate, chorusDepth, chorusMix);
        }
        else if (nameTwister == "chorusDepth")
        {
            chorusDepth = Mathf.Abs((gameObject.transform.eulerAngles.z - 360) / 270);
            if (chorusToggle.isOn)
                pedalManager.GetComponent<PedalManager>().ValuesOfChorusEffect(chorusRate, chorusDepth, chorusMix);
        }
        else if (nameTwister == "chorusMix")
        {
            chorusMix = Mathf.Abs((gameObject.transform.eulerAngles.z - 360) / 270);
            if (chorusToggle.isOn)
                pedalManager.GetComponent<PedalManager>().ValuesOfChorusEffect(chorusRate, chorusDepth, chorusMix);
        }
        else if (nameTwister == "delayDelay")
        {
            delayDelay = Mathf.Abs((gameObject.transform.eulerAngles.z - 360) / 270) * 5000;
            if (delayToggle.isOn)
                pedalManager.GetComponent<PedalManager>().ValuesOfDelayEffect(delayDelay, feedbackDelay, mixDelay);
        }
        else if (nameTwister == "feedbackDelay")
        {
            feedbackDelay = Mathf.Abs((gameObject.transform.eulerAngles.z - 360) / 270);
            if (delayToggle.isOn)
                pedalManager.GetComponent<PedalManager>().ValuesOfDelayEffect(delayDelay, feedbackDelay, mixDelay);
        }
        else if (nameTwister == "delayMix")
        {
            mixDelay = Mathf.Abs((gameObject.transform.eulerAngles.z - 360) / 270);
            if (delayToggle.isOn)
                pedalManager.GetComponent<PedalManager>().ValuesOfDelayEffect(delayDelay, feedbackDelay, mixDelay);
        }
        else if(nameTwister == "tremoloDepth")
        {
            tremoloDepth = Mathf.Abs((gameObject.transform.eulerAngles.z - 360) / 270)*0.5f;
            if(tremoloToggle.isOn)
            {
                pedalManager.GetComponent<PedalManager>().ValuesOfTremoloEffect(tremoloDepth, tremoloRate);
            }
        }
        else if(nameTwister == "tremoloRate")
        {
            tremoloRate = Mathf.Abs((gameObject.transform.eulerAngles.z - 360) / 270)*15;
            if (tremoloToggle.isOn)
            {
                pedalManager.GetComponent<PedalManager>().ValuesOfTremoloEffect(tremoloDepth, tremoloRate);
            }
        }



    }

    private void Update()
    {
        
    }


}
