﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckAdsBought : MonoBehaviour
{

    public float bot, top, nobot, notop;

    private void OnEnable()
    {
        RectTransform rectTransform = GetComponent<RectTransform>();
        if (PlayerPrefs.GetInt(AppodealServiceScript.NO_BANNER_INTER_KEY, 0) == 0)
        {
            SetBotAndTop(rectTransform, bot, top);
        }
        else
        {
            SetBotAndTop(rectTransform, nobot, notop);
        }
    }

    private void SetBotAndTop(RectTransform rectTransform, float bottom, float top)
    {
        rectTransform.offsetMin = new Vector2(rectTransform.offsetMin.x, bottom);
        rectTransform.offsetMax = new Vector2(rectTransform.offsetMax.x, -top);
    }
}
