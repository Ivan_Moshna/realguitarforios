﻿using System.Collections;
using System.Collections.Generic;
using LitJson;
using UnityEngine;

public class ChordsGenerator : MonoBehaviour{
    
    private JsonData ChordsData;
    
    Chord _chord;

    public static Chord[] chords;


    public static Chord GetChord(string chordName)
    {
        for (int i = 0; i < chords.Length; i++)
        {
            if (chords[i].name == chordName)
            {
                return chords[i];
            }
        }
        return null; // pizda 
    }

    void Start()
    {
        string jsonString = Resources.Load<TextAsset>("Chords").ToString();

        Chordss chordsFromJson = JsonUtility.FromJson<Chordss>(jsonString);
        chords = new Chord[chordsFromJson.Chords.Length];
        for (int i = 0; i < chords.Length; i++)
        {
            chords[i] = chordsFromJson.Chords[i];
        }

    }
}
