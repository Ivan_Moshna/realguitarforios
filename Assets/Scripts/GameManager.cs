﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using DG.Tweening;


public class GameManager : MonoBehaviour
{
   
    public static GameManager Instance { get; private set;}

    public GameObject startPanel;
    public GameObject backgroundPanel;
    public GameObject rateUsPanel;
    public GameObject FXPanel;

    public Sprite stratocasterImage;
    public Sprite acousticImage;
    public Sprite lespaulImage;

    public Image FLStrat;
    public Image FLAcoustic;
    public Image FLLesPaul;

    public Image stratButton;
    public Image acousticButton;
    public Image lespaulButton;

    public Sprite[] acousticStringImage;
    public Sprite[] electroStringImage;

    public AudioSource[] playSourses;

    public string pathName;

    public float fadeTime;

    private int numberOfStarts = 0;
    private Color startColor = new Color(1f,1f,1f,0f);


    private void Awake()
    {
        numberOfStarts = PlayerPrefs.GetInt("Starts", 0);
        Debug.Log("numberOfStarts " + numberOfStarts);
        PlayerPrefs.SetInt("Starts", ++numberOfStarts);
        Debug.Log("numberOfStarts " + numberOfStarts);
        
    }

    //private void OnApplicationQuit()
    //{
    //    PlayerPrefs.DeleteAll();
    //}

    public void OnNeverRateUsButtonClick()
    {
        PlayerPrefs.SetInt("StateRateUs", 0);
        rateUsPanel.SetActive(false);
    }

    public void OnLaterRateUsButtonClick()
    {
        numberOfStarts = 0;
        PlayerPrefs.SetInt("Starts", 0);
        Debug.Log("Later " + numberOfStarts);
        rateUsPanel.SetActive(false);
        rateUsPanel.GetComponent<Image>().color = startColor;
    }

    public void OnRateUsButtonClick()
    {
        PlayerPrefs.SetInt("StateRateUs", 0);
        rateUsPanel.SetActive(false);
        Application.OpenURL("https://play.google.com/store/apps/details?id=com.mobiray.realguitar3&hl=ru");
    }

    public void OnStratButtonClick()
    {
        acousticButton.raycastTarget = false;
        lespaulButton.raycastTarget = false;
        FLStrat.DOFade(1.0f, fadeTime).OnComplete(()=>StratCallback());
        
    }

    public TweenCallback StratCallback()
    {
        backgroundPanel.GetComponent<Image>().sprite = stratocasterImage;
        pathName = "Sounds/Stratocaster/";
        SoundManagerScript.Instance.LoadChord("open");

        for (int i = 0; i < SoundManagerScript.Instance.stringImage.Length; i++)
        {
            SoundManagerScript.Instance.stringImage[i].sprite = electroStringImage[i];
        }

        startPanel.SetActive(false);
        ReplacePedalScript.Instance.StratocasterSelected();

        if (numberOfStarts >= 3 && PlayerPrefs.GetInt("StateRateUs", 1) == 1)
        {
            rateUsPanel.SetActive(true);
            rateUsPanel.GetComponent<Image>().DOFade(0.7f, fadeTime);
        }
        FLStrat.color = startColor;

        acousticButton.raycastTarget = true;
        lespaulButton.raycastTarget = true;

        return null;
    }

    public void OnAcousticButtonClick()
    {
        stratButton.raycastTarget = false;
        lespaulButton.raycastTarget = false;
        FLAcoustic.DOFade(1.0f, fadeTime).OnComplete(() => AcousticCallback());
    }
    public TweenCallback AcousticCallback()
    {
        backgroundPanel.GetComponent<Image>().sprite = acousticImage;
        pathName = "Sounds/Acoustic/";
        SoundManagerScript.Instance.LoadChord("open");

        for (int i = 0; i < SoundManagerScript.Instance.stringImage.Length; i++)
        {
            SoundManagerScript.Instance.stringImage[i].sprite = acousticStringImage[i];
        }

        startPanel.SetActive(false);
        ReplacePedalScript.Instance.AcousticSelected();
        if (numberOfStarts >= 3 && PlayerPrefs.GetInt("StateRateUs", 1) == 1)
        {
            rateUsPanel.SetActive(true);
            rateUsPanel.GetComponent<Image>().DOFade(0.7f, fadeTime);

        }
        FLAcoustic.color = startColor;

        stratButton.raycastTarget = true;
        lespaulButton.raycastTarget = true;

        return null;
    }

    public void OnLespaulButtonClick()
    {
        acousticButton.raycastTarget = false;
        stratButton.raycastTarget = false;
        FLLesPaul.DOFade(1.0f, fadeTime).OnComplete(() => LesPaulCallback());
    }

    public TweenCallback LesPaulCallback()
    {

        backgroundPanel.GetComponent<Image>().sprite = lespaulImage;
        pathName = "Sounds/LesPaul/";
        SoundManagerScript.Instance.LoadChord("open");

        for (int i = 0; i < SoundManagerScript.Instance.stringImage.Length; i++)
        {
            SoundManagerScript.Instance.stringImage[i].sprite = electroStringImage[i];
        }

        startPanel.SetActive(false);
        ReplacePedalScript.Instance.LesPaulSelected();
        if (numberOfStarts >= 3 && PlayerPrefs.GetInt("StateRateUs", 1) == 1)
        {
            rateUsPanel.SetActive(true);
            rateUsPanel.GetComponent<Image>().DOFade(0.7f, fadeTime);

        }
        FLLesPaul.color = startColor;

        acousticButton.raycastTarget = true;
        stratButton.raycastTarget = true;

        return null;
    }

    public void OnChooseButtonClick()
    {
        startPanel.SetActive(true);
        for (int i = 0; i < playSourses.Length; i++)
        {
           playSourses[i].Stop();
        }
        ReplacePedalScript.Instance.distortionPedal.GetComponentInChildren<Toggle>().isOn = false;
        ReplacePedalScript.Instance.chorusPedal.GetComponentInChildren<Toggle>().isOn = false;
        ReplacePedalScript.Instance.tremoloPedal.GetComponentInChildren<Toggle>().isOn = false;
        ReplacePedalScript.Instance.delayPedal.GetComponentInChildren<Toggle>().isOn = false;

    }

    private void Start()
    {
        Instance = this;

        startPanel.SetActive(true);

        
    }


}
