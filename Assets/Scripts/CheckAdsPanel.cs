﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckAdsPanel : MonoBehaviour
{

    private void OnEnable()
    {
        RectTransform rectTransform = GetComponent<RectTransform>();
        if (PlayerPrefs.GetInt(AppodealServiceScript.NO_BANNER_INTER_KEY, 0) == 0)
        {
            gameObject.SetActive(true);
        }
        else
        {
            gameObject.SetActive(false);
        }
    }
}